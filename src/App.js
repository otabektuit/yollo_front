import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import { ProductDetails } from "./pages/productDetails/productDetails";
import Product from "./pages/product";
import ByPass from "./pages/byPass";
import Home from "./pages/home";

function App() {
  return (
    <div className="App">
      <Router>
        {/* <Navbar /> */}
        <Routes>
          <Route index element={<Home />} />
          <Route path="/product/byPassLink/:id" element={<ByPass />} />
          <Route path="/product/:id" element={<Product />} />
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
