import React from "react";
import { Link, useLocation } from "react-router-dom";

import "./navbar.css";

export const Navbar = () => {
  const location = useLocation();

  return (
    <div className="navbar">
      <div className="logo">
        <Link to="#" id="NavTitle">
          Yollo
        </Link>
      </div>
    </div>
  );
};
