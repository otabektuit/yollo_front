import axios from "axios";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import styles from "./style.module.scss";
import ReactElasticCarousel from "react-elastic-carousel";
import { useEffect, useState } from "react";

import classNames from "classnames";
import { Heart } from "phosphor-react";
import { getBrowserLocales } from "../../utils";
import "video-react/dist/video-react.css";
import ReactStars from "react-rating-stars-component";
import { Helmet } from "react-helmet";
import Slider from "react-slick";
import { Player } from "video-react";
import { NumericFormat } from "react-number-format";

const Product = () => {
  const { id } = useParams();

  const [lang, setLang] = useState("en");
  const [isExpanded, setIsExpanded] = useState(false);
  const [isLiked, setIsLiked] = useState(localStorage.getItem("isLiked"));

  const { data, isLoading } = useQuery("GET_PRODUCT", () =>
    axios.get("https://api.yollo.io/api/v1/product/client/?productId=" + id)
  );

  useEffect(() => {
    const item = navigator.language || navigator.userLanguage;

    if (item.slice(0, 2) === "ko") setLang("kr");
  }, []);

  const onBuy = () => {
    axios
      .post("https://api.yollo.io/api/v1/product/client/buy", {
        productId: id
      })
      .then((res) => {
        window.location.replace(res.data.data);
      });
  };

  const handleLike = () => {
    if (!isLiked)
      axios
        .post("https://api.yollo.io/api/v1/product/client/likes", {
          productId: id,
          addLikeDislike: "like"
        })
        .then((res) => {
          setIsLiked((prev) => !prev);
        });
  };

  if (isLoading) return <div>Loading...</div>;

  if (!data) {
    return <div>Product not found</div>;
  }

  return (
    <>
      <Helmet
        script={[
          {
            type: "",
            innerHTML: `!(function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
              n.callMethod
                ? n.callMethod.apply(n, arguments)
                : n.queue.push(arguments);
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = "2.0";
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
          })(
            window,
            document,
            "script",
            "https://connect.facebook.net/en_US/fbevents.js"
          );
          fbq("init", "${data.data?.data?.pixelID}");
          fbq("track", "PageView");`
          }
        ]}
      ></Helmet>
      <div className={styles.container}>
        <div
          className={classNames(styles.images, {
            [styles.isExpanded]: isExpanded
          })}
          id="images"
        >
          {/* <div
            className={styles.overlay}
            onClick={() => setIsExpanded((prev) => !prev)}
          /> */}
          <ReactElasticCarousel showArrows={false}>
            {data?.data?.data?.productImages?.map((img) => (
              <div className={styles.image}>
                <img src={img} alt="product" />
              </div>
            ))}
          </ReactElasticCarousel>
        </div>
        <div
          className={classNames(styles.infos, {
            [styles.isExpanded]: isExpanded
          })}
        >
          <div
            className={styles.swipe_box}
            onClick={() => setIsExpanded((prev) => !prev)}
          >
            {/* <span className={styles.swiper}></span> */}
          </div>
          <div className={styles.actions}>
            <button className={styles.like} onClick={handleLike}>
              <Heart size={24} weight={isLiked ? "fill" : "regular"} />
            </button>
            <p>
              <span>
                <NumericFormat
                  value={data?.data?.data?.currentPrice}
                  displayType="text"
                  thousandSeparator={true}
                  decimalScale={3}
                />{" "}
              </span>{" "}
              <span>{lang === "en" ? "KRW" : "원"}</span>
            </p>
            <button className={styles.buy} onClick={onBuy}>
              {lang === "en" ? "SEE PRODUCT" : "제품 보기"}
            </button>
          </div>
          <div
            className={styles.box}
            // onClick={() => setIsExpanded((prev) => !prev)}
          >
            <div className={styles.meta}>
              <div className={styles.pricing}>
                {data?.data?.data?.discount && (
                  <p>Sale {data?.data?.data?.discount}%</p>
                )}
              </div>
              <h1>{data?.data?.data?.productName?.[lang]}</h1>
              <div className={styles.rates}>
                <ReactStars
                  count="5"
                  // onChange={ratingChanged}
                  value={data?.data?.data?.stars}
                  size={20}
                  edit={false}
                />
                <p>({data?.data?.data?.productPurchuased})</p>
              </div>
            </div>

            <div
              className={styles.desciption}
              dangerouslySetInnerHTML={{
                __html: data?.data?.data?.description?.[lang]
              }}
            ></div>

            <div className={styles.videos}>
              {data?.data?.data?.videoUrl?.map((video, v) => (
                <iframe width="100%" height="315" src={video} key={v}></iframe>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Product;
