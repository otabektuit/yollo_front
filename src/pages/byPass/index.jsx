import axios from 'axios'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export default function ByPass() {
  const { id } = useParams()
  const getLink = async () => {
    try {
      const res = await axios.get(
        `https://api.yollo.io/api/v1/product/client/byPassLink?productId=${id}`
      )
      if (res.data.data) {
        window.open(res.data.data.productUrl, '_self')
      }
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    getLink()
  }, [])

  return <div></div>
}
