import { NumericFormat } from "react-number-format";
import styles from "./style.module.scss";
import ReactStars from "react-rating-stars-component";
import { useQuery } from "react-query";
import axios from "axios";
import { useEffect, useState } from "react";

const Home = () => {
  const [lang, setLang] = useState("en");
  const { data } = useQuery("GET_PRODUCTS", () =>
    axios.get(
      "https://api.yollo.io/api/v1/product/client/getProductList?page=1&perPage=20"
    )
  );

  useEffect(() => {
    const item = navigator.language || navigator.userLanguage;
    if (item.slice(0, 2) === "ko") setLang("kr");
  }, []);

  const products = data?.data?.data?.results;

  return (
    <>
      <div className={styles.container}>
        <div className={styles.products}>
          {products?.map((product, p) => (
            <div className={styles.product} key={p}>
              <div className={styles.header}>
                <img src={product.productImages[0]} alt="Product" />
              </div>
              <div className={styles.body}>
                <div className={styles.meta}>
                  <h3>{product.productName[lang]}</h3>
                  <NumericFormat
                    value={product.currentPrice}
                    displayType="text"
                    thousandSeparator={true}
                    decimalScale={3}
                    suffix="KWR"
                  />
                </div>
                <p className={styles.marketName}>{product.marketName}</p>
                <a className={styles.link} href={product.productUrl}>
                  SEE PRODUCT
                </a>
              </div>
            </div>
          ))}
        </div>
      </div>

      <footer className={styles.footer}>
        <div className={styles.content}>
          <h3>Email: support@yollo.io</h3>
          <p>
            Compensation Statement: We're partnered with select platforms, and
            we receive support accordingly.
          </p>
        </div>
        <div className={styles.rights}>
          <h3>© 2023 yollo.io. All rights reserved.</h3>
        </div>
      </footer>
    </>
  );
};

export default Home;
