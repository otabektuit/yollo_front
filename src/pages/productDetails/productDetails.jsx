import React from "react";
import { useParams } from "react-router-dom";

import "./productDetails.css";
import { Bag, Star } from "phosphor-react";
import { useQuery } from "react-query";
import axios from "axios";
import ReactElasticCarousel from "react-elastic-carousel";

export const ProductDetails = () => {
  const { id } = useParams();

  const { data, isLoading } = useQuery("GET_PRODUCT", () =>
    axios.get("https://api.yollo.io/api/v1/product/client/?productId=" + id)
  );
  console.log(data);
  if (isLoading) return <div>Loading...</div>;

  if (!data) {
    return <div>Product not found</div>;
  }

  return (
    <div className="product-container">
      <div className="product-image-container">
        <ReactElasticCarousel
          verticalMode
          pagination={false}
          showArrows={false}
        >
          {data?.data?.data?.productImages?.map((img) => (
            <div
              style={{
                width: "100vw",
                height: "80vh"
              }}
            >
              <img
                src={img}
                alt="product"
                height="100%"
                width="100%"
                style={{
                  objectFit: "cover"
                }}
              />
            </div>
          ))}
        </ReactElasticCarousel>
      </div>
      <div className="product-details-container">
        <div className="product-header">
          <h1 className="product-name">{data?.data?.data?.productName?.en} </h1>
          <div className="product-purchased">
            <span>
              <Star size={32} /> {data?.data?.data?.stars}
            </span>
            <span>
              <Bag size={32} /> {data?.data?.data?.productPurchuased}
            </span>
          </div>
        </div>

        <div className="product-prices">
          <p>${data?.data?.data?.currentPrice}</p>
          {!!data?.data?.data?.discount && (
            <span>
              -{data?.data?.data?.discount}% <b>Sale</b>
            </span>
          )}
        </div>

        <div className="product-details">
          <p className="product-description">
            {data?.data?.data?.description?.en}
          </p>
          {/* <div className="proudct-images">
            {data?.data?.data?.productImages?.map((img, i) => (
              // eslint-disable-next-line jsx-a11y/img-redundant-alt
              <img src={img} alt="product image" width={200} key={i} />
            ))}
          </div> */}

          <a
            href={data?.data?.data?.productUrl}
            target="_blank"
            rel="noreferrer"
          >
            <button className="purchaseBtn">
              <Bag size={26} />
              <span>Request to Order</span>
            </button>
          </a>
        </div>
      </div>
    </div>
  );
};
